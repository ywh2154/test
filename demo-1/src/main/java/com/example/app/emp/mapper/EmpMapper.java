package com.example.app.emp.mapper;

import java.util.List;

import com.example.app.emp.service.EmpVO;

public interface EmpMapper {

	public List<EmpVO> selectAllEmpList(); //리스트 출력
	
	public int insertEmpInfo(EmpVO empvo); //employee 넣기
	
	public EmpVO selectEmpInfo(int empId);
	
}
