package com.example.app.emp.service;

import java.util.List;

public interface EmpService {

	public List<EmpVO> getAllList();
	public int insertEmpInfo(EmpVO empvo);
	public EmpVO getEmpInfo(int empId);
}
