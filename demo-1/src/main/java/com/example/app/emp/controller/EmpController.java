package com.example.app.emp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.app.emp.service.EmpService;
import com.example.app.emp.service.EmpVO;

@Controller
public class EmpController {

	@Autowired
	EmpService empService;
	
	@RequestMapping(value="/empList", method=RequestMethod.GET) //get방식으로만 쓸때
	//@GetMapping("empList") 위랑 같은 의미
	public String empAllList(Model model) {
	
		model.addAttribute("empList", empService.getAllList());
		return "empList"; //tiles는 사용하지 않음. resources 밑에있는 empList.html파일을 찾음.
	}
	
	//입력을 처리하는페이지, 입력하는페이지 2개
	@GetMapping("/empInsert")
	public String inputEmpForm() {
		//입력하는페이지
		return "empInsert";
	}
	
	@PostMapping("/empInsert")
	public String inputEmpProcess(EmpVO empVO) {
		//처리
		empService.insertEmpInfo(empVO);
		return "redirect:empList";//해당 페이지 호출 
	}
}
