package com.example.app.emp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.app.emp.mapper.EmpMapper;
import com.example.app.emp.service.EmpService;
import com.example.app.emp.service.EmpVO;

@Service
public class EmpServiceImpl implements EmpService {

	@Autowired
	EmpMapper empMapper;
	
	@Override
	public List<EmpVO> getAllList() {
		
		return empMapper.selectAllEmpList();
	}

	@Override
	public int insertEmpInfo(EmpVO empvo) {
		// TODO Auto-generated method stub
		//사원번호를 구한 다음에 insert처리를 해도 됨.
		
		//사원정보 등록
		int result = empMapper.insertEmpInfo(empvo);
		return result;
	}

	@Override
	public EmpVO getEmpInfo(int empId) {
		// TODO Auto-generated method stub
		return empMapper.selectEmpInfo(empId);
	}

}
