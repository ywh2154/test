package com.example.app.emp.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EmpVO {
//hr의 employees
	String employeeId;
	String firstName;
	String lastName;
	String email;
	@DateTimeFormat(pattern="yyyy-mm-dd")
	Date hireDate;
	String jobId;
	int departmentId;
	
	
}
