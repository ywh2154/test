package com.example.demo;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = Demo1Application.class)
class Demo1Application {

	@Test
	void contextLoads() {
		
		String[] dataList= {"jdbc:oracle:thin:@3.34.193.43:1521/xe","hr","hr"};
		
		
	        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
	        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
	        config.setPassword("won");
	        config.setAlgorithm("PBEWithMD5AndDES");
	        config.setKeyObtentionIterations("1000");
	        config.setPoolSize("1");
	        config.setProviderName("SunJCE");
	        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
	        config.setIvGeneratorClassName("org.jasypt.iv.NoIvGenerator");
	        config.setStringOutputType("base64");
	        encryptor.setConfig(config);
	        
	        for(String data: dataList) {
	        	String encrypText = encryptor.encrypt(data);
	        	System.out.println(encrypText);
	        }
	    
	}

}
